﻿namespace VisitorDemo
{
    public class Person
    {
        public string Name { get; set; }
        public Gender Gender { get; set; }
    }
}
