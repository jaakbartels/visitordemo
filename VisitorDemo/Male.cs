﻿namespace VisitorDemo
{
    public class Male : Gender
    {
        public Male() : base("male")
        {
        }

        public override T AcceptVisitor<T>(IGenderVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}