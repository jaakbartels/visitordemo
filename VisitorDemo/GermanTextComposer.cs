﻿namespace VisitorDemo
{
    public class GermanTextComposer : IGenderVisitor<string>
    {
        public string GetText(Person person)
        {
            return $"{person.Name} hat {person.Gender.AcceptVisitor(this)} Profilbild aktualisiert";
        }

        public string Visit(Male gender)
        {
           return "sein";
        }

        public string Visit(Female gender)
        {
            return "ihr";
        }

        public string Visit(Undisclosed gender)
        {
            return "ihr/sein";
        }
    }
}