﻿namespace VisitorDemo
{
    public class FrenchTextComposer : IGenderVisitor<string>
    {
        private string _possessiveAdjective;

        public string GetText(Person person)
        {
            return $"{person.Name} a changé sa photo de profil";
        }

        public string Visit(Male gender)
        {
            throw new System.NotImplementedException();
        }

        public string Visit(Female gender)
        {
            throw new System.NotImplementedException();
        }

        public string Visit(Undisclosed gender)
        {
            throw new System.NotImplementedException();
        }
    }
}