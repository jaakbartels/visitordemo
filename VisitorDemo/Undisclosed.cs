﻿using System.Runtime.InteropServices.ComTypes;

namespace VisitorDemo
{
    public class Undisclosed : Gender
    {
        public Undisclosed() : base("undisclosed")
        {
        }

        public override T AcceptVisitor<T>(IGenderVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
