﻿namespace VisitorDemo
{
    public interface IGenderVisitor<out T>
    {
        T Visit(Male gender);
        T Visit(Female gender);
        T Visit(Undisclosed gender);
    }
}