﻿using System.Drawing;

namespace VisitorDemo
{
    public class ColorSelector : IGenderVisitor<Color>
    {
        private Color _color;

        public Color GetColor(Person person)
        {
            return person.Gender.AcceptVisitor(this);
        }

        public Color Visit(Male gender)
        {
            return Color.Blue;
        }

        public Color Visit(Female gender)
        {
            return Color.Pink;
        }

        public Color Visit(Undisclosed gender)
        {
            return Color.Green;
        }
    }
}