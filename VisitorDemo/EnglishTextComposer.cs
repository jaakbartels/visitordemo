﻿namespace VisitorDemo
{
    public class EnglishTextComposer : IGenderVisitor<string>
    {
        public string GetText(Person person)
        {
            return $"{person.Name} changed {person.Gender.AcceptVisitor(this)} photo";
        }

        public string Visit(Male gender)
        {
            return "his";
        }

        public string Visit(Female gender)
        {
            return "her";
        }

        public string Visit(Undisclosed gender)
        {
            return "his/her";
        }
    }
}
