﻿namespace VisitorDemo
{
    public class Female : Gender
    {
        public Female() : base("female")
        {
        }

        public override T AcceptVisitor<T>(IGenderVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }

    }
}