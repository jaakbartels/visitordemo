﻿using System.Collections.Generic;
using System.Linq;

namespace VisitorDemo
{
    public abstract class Gender
    {
        protected Gender(string code)
        {
            Code = code;
        }

        public string Code { get; }

        public static Gender Male = new Male();
        public static Gender Female = new Female();
        public static Gender Undisclosed = new Undisclosed();

        public static List<Gender> All = new List<Gender> { Male, Female, Undisclosed };

        private static Dictionary<string, Gender> _byCode = All.ToDictionary(x => x.Code);

        public Gender FromCode(string code)
        {
            return _byCode[code];
        }

        public abstract T AcceptVisitor<T>(IGenderVisitor<T> visitor);
    }
}
