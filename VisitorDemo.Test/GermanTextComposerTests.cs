﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VisitorDemo.Test
{
    [TestClass]
    public class GermanTextComposerTests
    {
        [TestMethod]
        public void GetTextReturnsMaleVersion()
        {
            //Arrange
            var person = new Person{Name = "Rudi", Gender=Gender.Male};

            //Act
            var sut = new GermanTextComposer();
            var result = sut.GetText(person);

            //Assert
            Assert.AreEqual("Rudi hat sein Profilbild aktualisiert", result);
        }        
        
        [TestMethod]
        public void GetTextReturnsFemaleVersion()
        {
            //Arrange
            var person = new Person{Name = "Angela", Gender=Gender.Female};

            //Act
            var sut = new GermanTextComposer();
            var result = sut.GetText(person);

            //Assert
            Assert.AreEqual("Angela hat ihr Profilbild aktualisiert", result);
        }
    }
}