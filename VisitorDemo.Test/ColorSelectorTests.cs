﻿using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VisitorDemo.Test
{
    [TestClass]
    public class ColorSelectorTests
    {
        [TestMethod]
        public void ReturnsBlueForMale()
        {
            //Arrange
            var person = new Person {Name = "Mark", Gender = Gender.Male};

            //Act
            var sut = new ColorSelector();
            var result = sut.GetColor(person);

            //Assert
            Assert.AreEqual(Color.Blue, result);
        }

        [TestMethod]
        public void ReturnsPinkForFemale()
        {
            //Arrange
            var person = new Person {Name = "Maria", Gender = Gender.Female};

            //Act
            var sut = new ColorSelector();
            var result = sut.GetColor(person);

            //Assert
            Assert.AreEqual(Color.Pink, result);
        }
    }
}
