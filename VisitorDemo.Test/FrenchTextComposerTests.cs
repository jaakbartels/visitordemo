﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VisitorDemo.Test
{
    [TestClass]
    public class FrenchTextComposerTests
    {
        [TestMethod]
        public void GetTextReturnsMaleVersion()
        {
            //Arrange
            var person = new Person{Name = "Patrick", Gender=Gender.Male};

            //Act
            var sut = new FrenchTextComposer();
            var result = sut.GetText(person);

            //Assert
            Assert.AreEqual("Patrick a changé sa photo de profil", result);
        }        
        
        [TestMethod]
        public void GetTextReturnsFemaleVersion()
        {
            //Arrange
            var person = new Person{Name = "Nathalie", Gender=Gender.Female};

            //Act
            var sut = new FrenchTextComposer();
            var result = sut.GetText(person);

            //Assert
            Assert.AreEqual("Nathalie a changé sa photo de profil", result);
        }
    }
}