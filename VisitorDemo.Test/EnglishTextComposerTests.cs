﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VisitorDemo.Test
{
    [TestClass]
    public class EnglishTextComposerTests
    {
        [TestMethod]
        public void GetTextReturnsMaleVersion()
        {
            //Arrange
            var person = new Person{Name = "Mark", Gender=Gender.Male};

            //Act
            var sut = new EnglishTextComposer();
            var result = sut.GetText(person);

            //Assert
            Assert.AreEqual("Mark changed his photo", result);
        }        
        
        [TestMethod]
        public void GetTextReturnsFemaleVersion()
        {
            //Arrange
            var person = new Person{Name = "Maria", Gender=Gender.Female};

            //Act
            var sut = new EnglishTextComposer();
            var result = sut.GetText(person);

            //Assert
            Assert.AreEqual("Maria changed her photo", result);
        }
    }
}
